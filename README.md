# nsupdate-cron

Simple nsupdate-fedcloud script to regularly check and update the site domain.

You need to create config file, nsupdate-fedcloud.ini, in the SAME directory as where you place the main script, nsupdate-fedcloud.sh

Please, see example: nsupdate-fedcloud-EXAMPLE.ini
