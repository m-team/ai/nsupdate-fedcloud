#!/bin/bash
###
# Shell script to update Domain Name  with the right IP
# One HAS TO provide either 'service_secret' and 'service_site' below
# OR define SERVICE_SECRET and SERVICE_SITE environment settings!
#
# vkozlov@2023
# Copyright 2023 Karlsruhe Institue of Technology
###

###
# this Script full path
# https://unix.stackexchange.com/questions/17499/get-path-of-current-script-when-executed-through-a-symlink/17500
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

# load default config
# loads: service_site, service_secret, service_logfile
source ${SCRIPT_PATH}/nsupdate-fedcloud.ini

# function to check if SITE responds, if not => update IP via nsupdate service
check_hostname()
{
  site=$1
  secret=$2

  response=$(curl -s -o /dev/null -w "%{http_code}" --connect-timeout 15  https://${site})

  if [ $response -eq 200 ]; then
     message="is responding, do NOT update IP"
  else
     message=$(curl https://${site}:${secret}@nsupdate.fedcloud.eu/nic/update)
  fi
  echo "${site}: CODE ${response} : ${message}"
}

# one can re-define SERVICE_SECRET and SERVICE_SITE via Environment Variables
# if not defined => use default values provided above
if [ ${#SERVICE_SECRET} -le 1 ]; then
  SERVICE_SECRET=${service_secret}
fi
if [ ${#SERVICE_SITE} -le 1 ]; then 
  SERVICE_SITE=${service_site}
fi

# check the hostname and log results
echo $(date +"%F %T %Z") >> ${service_logfile}
check_hostname $SERVICE_SITE $SERVICE_SECRET >> ${service_logfile}
